from get_data import get_stats

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
from dash.dependencies import Input, Output

####GETTING DF - getting DF in different function as there might be added some filtering in the future which depends on dropdown
def read_data():
    df = get_stats()
    return df

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css', "https://codepen.io/chriddyp/pen/brPBPO.css"]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(children=[
    html.H1(children='Data Load'),

    html.Div(children='''
        Real time data for data load.
    '''),
    html.Div([
        dcc.Dropdown(
            id='chart-selection',
            options = [{"label": 'total number of rows over time', "value": "lm_n_rows"},{"label": 'number of rows within 30 sec interval', "value": 'lm_rows_per_s'}],
            value = 'lm_n_rows'
        )
    ]),

    dcc.Graph(
        id='data-graph'
    ),
    dcc.Interval(
            id='interval-component',
            interval=5*1000, # in milliseconds
            n_intervals=0
        )
])

@app.callback(Output('data-graph', 'figure'),
              [Input('interval-component', 'n_intervals'),
              Input('chart-selection', 'value')])
def update_graph(n, selected_chart):
    df = read_data()
    y_axis = selected_chart
    return go.Figure([go.Scatter(x=df['lm_dt'], y=df[y_axis])])


if __name__ == '__main__':
    app.run_server(debug=True)


