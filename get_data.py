import pyodbc
import pandas as pd


########STATIC TEST#############
def get_stats():
    return pd.read_csv('ldmeter_data.csv').fillna(0)

# ########DYNAMIC SQL QUERY#############
# def get_stats():
#     ###TEST connection
#     connection = pyodbc.connect('DRIVER=/Library/ODBC/OpenLink Virtuoso ODBC Driver.bundle/Contents/MacOS/virtodbc_r.so;HOST=localhost:1111;UID=dba;PWD=test')

#     connection.setdecoding(pyodbc.SQL_CHAR, encoding='utf-8')
#     connection.setdecoding(pyodbc.SQL_WCHAR, encoding='utf-8')
#     connection.setencoding(encoding='utf-8')

#     query = "SELECT lm_id, lm_dt, lm_secs_since_start, lm_n_rows, lm_rows_per_s FROM ld_metric)

#     sql_query = pd.read_sql_query(query, connection).fillna(0)

#     return sql_query

#     # print(sql_query.head())


